package club.facecard.facecard;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;


public class ConfirmActivity extends AppCompatActivity {
    Bitmap bitmap;
    Bitmap bimap;
    String token;
    String id = "";
    private boolean register = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);
        ImageView photo = findViewById(R.id.photo);
        File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                "picture.jpg");
        bimap = BitmapFactory.decodeFile(file.getAbsolutePath());
        bitmap = flip(bimap);
        photo.setImageBitmap(bitmap);
        Intent intent = getIntent();


        id = intent.getStringExtra("id");
        token = intent.getStringExtra("token");
        if (token != null) {
            register = true;
        }

    }

    Bitmap flip(Bitmap src) {
        Matrix m = new Matrix();
        m.preScale(-1, 1);
        Bitmap dst = Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), m, false);
        dst.setDensity(DisplayMetrics.DENSITY_DEFAULT);
        return dst;
    }

    FloatingActionButton fab;
    Button bb;
    ProgressBar pr;

    public void sendPicture(View view) {
        fab = findViewById(R.id.take_picture);
        pr = findViewById(R.id.progress);
        bb = findViewById(R.id.go_back);
        AsyncTask.execute(new Runnable() {

            @Override
            public void run() {

                fab.post(new Runnable() {
                    public void run() {
                        fab.setVisibility(View.GONE);
                        bb.setVisibility(View.GONE);

                        pr.setVisibility(View.VISIBLE);
                    }
                });
                try {
                    String URL;
                    if (!register) {
                        URL = "https://pu0a5axu81.execute-api.us-west-2.amazonaws.com/v2/sendmessage/base64";
                    } else {
                        URL = "https://pu0a5axu81.execute-api.us-west-2.amazonaws.com/v2/registration/customers/photo/base64";
                    }


                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bimap = getResizedBitmap(bimap, 720);
                    bimap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    String encoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);


                    final RequestQueue requestQueue = Volley.newRequestQueue(ConfirmActivity.this);
                    JSONObject jsonBody = new JSONObject();
                    jsonBody.put("id", id);
                    if (register) jsonBody.put("token", token);
                    jsonBody.put("base64Photo", encoded);
                    JsonObjectRequest stringRequest = new JsonObjectRequest(
                            Request.Method.POST,
                            URL,
                            jsonBody, // the request body, which is a JsonObject otherwise null
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    String name = response.optString("name");
                                    Log.d("TAF", name);
                                    Log.d("TAF", response.toString());
                                    fab.post(new Runnable() {
                                        public void run() {
                                            fab.setVisibility(View.VISIBLE);
                                            bb.setVisibility(View.VISIBLE);
                                            pr.setVisibility(View.GONE);
                                        }
                                    });
                                    Toast toast;
                                    if (!register)
                                        toast = Toast.makeText(ConfirmActivity.this, "Hi, " + name, Toast.LENGTH_LONG);
                                    else
                                        toast = Toast.makeText(ConfirmActivity.this, "Successfully registered", Toast.LENGTH_LONG);

                                    toast.show();
                                    Intent intent = new Intent(ConfirmActivity.this, SuccessActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.putExtra("name", name);
                                    startActivity(intent);
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    fab.post(new Runnable() {
                                        public void run() {
                                            fab.setVisibility(View.VISIBLE);
                                            bb.setVisibility(View.VISIBLE);
                                            pr.setVisibility(View.GONE);
                                        }
                                    });
                                    if (error == null || error.networkResponse == null) {
                                        Toast toast = Toast.makeText(ConfirmActivity.this, "Connection Error", Toast.LENGTH_LONG);
                                        toast.show();
                                        return;
                                    }

                                    String body;
                                    //get status code here
                                    final String statusCode = String.valueOf(error.networkResponse.statusCode);
                                    //get response body and parse with appropriate encoding
                                    try {
                                        body = new String(error.networkResponse.data, "UTF-8");
                                        JSONObject jsonBody = new JSONObject(body);
                                        String err = jsonBody.getString("error_message");
                                        boolean shown = false;
                                        switch (err) {
                                            case "FCE009":
                                                //Wrong step
                                                shown = true;

                                                Intent intent = new Intent(ConfirmActivity.this, SuccessActivity.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                startActivity(intent);
                                                break;

                                        }
                                        if (!shown) {
                                            Log.d("ERROR", "error");
                                            Toast toast = Toast.makeText(ConfirmActivity.this, err, Toast.LENGTH_LONG);
                                            toast.show();
                                        }


                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    //   ConfirmActivity.this.startActivity( new Intent(ConfirmActivity.this, CameraActivity.class));
                                    finish();
                                }
                            }
                    );

                    requestQueue.add(stringRequest);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        //   float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleHeight, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }


    public void goBack(View view) {
        finish();
    }
}

