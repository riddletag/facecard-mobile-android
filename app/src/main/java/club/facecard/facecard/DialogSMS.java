package club.facecard.facecard;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;


public class DialogSMS extends Dialog implements
        View.OnClickListener {

    public Activity c;
    private EditText sms;

    DialogSMS(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
    }
    private OnSMSDialogResult smsDialogResult; // the callback

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_sms);
        sms = findViewById(R.id.sms_input);
        Button b = findViewById(R.id.sign_up_butt);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                smsDialogResult.finish(sms.getText().toString());
            }
        });
        //     no = (Button) findViewById(R.id.btn_no);
     //   yes.setOnClickListener(this);
     //   no.setOnClickListener(this);

    }


    void setSMSDialogResult(OnSMSDialogResult dialogResult){
        smsDialogResult = dialogResult;
    }

    @Override
    public void onClick(View v) {

    }


    public interface OnSMSDialogResult{
        void finish(String sms);
    }
}
