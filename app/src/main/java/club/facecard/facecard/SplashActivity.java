package club.facecard.facecard;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        final ImageView logo = findViewById(R.id.logo);
        final ImageView logoName = findViewById(R.id.logoname);
        final LinearLayout stepTwo = findViewById(R.id.steptwo);
        final RelativeLayout root = findViewById(R.id.root);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    TransitionManager.beginDelayedTransition(root);
                }
                logo.setVisibility(View.INVISIBLE);
                logoName.setVisibility(View.INVISIBLE);
                stepTwo.setVisibility(View.VISIBLE);
            }
        }, 1500);
    }



    DialogSignUp cdd;
    DialogSMS sms;
    DialogLogIn dialogLogIn;

    public void logIn(View view) {
       // startActivity(new Intent(this, CameraActivity.class));

        dialogLogIn = new DialogLogIn(SplashActivity.this);
        dialogLogIn.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        // Setting dialogview
        Window window = dialogLogIn.getWindow();
        window.setGravity(Gravity.TOP);

        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialogLogIn.setDialogResult(new DialogLogIn.OnMyDialogResult() {
            @Override
            public void finish(String name, String phone) {

                Register(name, filterPhone(phone));
            }
        });
        dialogLogIn.setCancelable(true);

        dialogLogIn.show();
    }



    public void signUp(View view) {
        cdd = new DialogSignUp(SplashActivity.this);
        cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        // Setting dialogview
        Window window = cdd.getWindow();
        window.setGravity(Gravity.TOP);

        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        cdd.setDialogResult(new DialogSignUp.OnMyDialogResult() {
            @Override
            public void finish(String name, String phone) {

                Register(name, filterPhone(phone));
            }
        });
        cdd.setCancelable(true);

        cdd.show();
    }

    public void enterSMS() {
        sms = new DialogSMS(SplashActivity.this);
        sms.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        // Setting dialogview
        Window window = sms.getWindow();
        window.setGravity(Gravity.TOP);

        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        sms.setSMSDialogResult(new DialogSMS.OnSMSDialogResult() {
            @Override
            public void finish(String SMS) {
                CheckSMS(SMS);
            }
        });
        sms.setCancelable(true);

        sms.show();
    }

    private void CheckSMS(String SMS) {
        try {

            String URL = "https://pu0a5axu81.execute-api.us-west-2.amazonaws.com/v2/registration/checkotp";

            final RequestQueue requestQueue = Volley.newRequestQueue(SplashActivity.this);
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("id", idd);
            jsonBody.put("code", SMS);
            jsonBody.put("token", tokenn);


            JsonObjectRequest stringRequest = new JsonObjectRequest(
                    Request.Method.POST,
                    URL,
                    jsonBody, // the request body, which is a JsonObject otherwise null
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            String id = response.optString("id");
                            String token = response.optString("token");

                            Log.d("TAF", id);
                            Log.d("TAF", token);

                            Log.d("TAF", response.toString());
                            sms.dismiss();
                            Intent i = new Intent(SplashActivity.this, CameraActivity.class);
                            i.putExtra("id",id);
                            i.putExtra("token",token);
                            startActivity(i);
                            //  enterSMS();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error == null || error.networkResponse == null) {
                                Toast toast = Toast.makeText(SplashActivity.this, "Connection Error", Toast.LENGTH_LONG);
                                toast.show();
                                return;
                            }

                            String body;
                            //get status code here
                          //  final String statusCode = String.valueOf(error.networkResponse.statusCode);
                            //get response body and parse with appropriate encoding
                            try {
                                body = new String(error.networkResponse.data, "UTF-8");
                                JSONObject jsonBody = new JSONObject(body);
                                String err = jsonBody.getString("error_code");
                                boolean shown = false;
                                switch (err) {
                                    case "FCE007":
                                        //Wrong format
                                        Toast toast1 = Toast.makeText(SplashActivity.this, "Wrong code", Toast.LENGTH_LONG);
                                        toast1.show();
                                        shown = true;
                                        break;

                                }
                                if (!shown) {
                                    Toast toast = Toast.makeText(SplashActivity.this, body, Toast.LENGTH_LONG);
                                    toast.show();
                                    Log.d("ERROR", body);
                                }


                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            );

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }





    String idd, tokenn;

    private void Register(String name, String phone) {

        try {

            String URL = "https://pu0a5axu81.execute-api.us-west-2.amazonaws.com/v2/registration/customers";

            final RequestQueue requestQueue = Volley.newRequestQueue(SplashActivity.this);
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("name", name);
            jsonBody.put("phone", phone);

            JsonObjectRequest stringRequest = new JsonObjectRequest(
                    Request.Method.POST,
                    URL,
                    jsonBody, // the request body, which is a JsonObject otherwise null
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            String id = response.optString("id");
                            String token = response.optString("token");

                            Log.d("TAF", id);
                            idd = id;
                            Log.d("TAF", token);
                            tokenn = token;
                            Log.d("TAF", response.toString());
                            cdd.dismiss();
                            enterSMS();

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error == null || error.networkResponse == null) {
                                Toast toast = Toast.makeText(SplashActivity.this, "Connection Error", Toast.LENGTH_LONG);
                                toast.show();
                                return;
                            }

                            String body;
                            //get status code here
                          //  final String statusCode = String.valueOf(error.networkResponse.statusCode);
                            //get response body and parse with appropriate encoding
                            try {
                                body = new String(error.networkResponse.data, "UTF-8");
                                JSONObject jsonBody = new JSONObject(body);
                                String err = jsonBody.getString("error_code");
                                boolean shown = false;
                                switch (err) {
                                    case "FCE012":
                                        //Wrong format
                                        Toast toast1 = Toast.makeText(SplashActivity.this, "Please enter phone correctly", Toast.LENGTH_LONG);
                                        toast1.show();
                                        shown = true;
                                        break;
                                    case "FCE013":
                                        //Cannot send SMS
                                        Toast toast2 = Toast.makeText(SplashActivity.this, "Cannot send an SMS", Toast.LENGTH_LONG);
                                        toast2.show();
                                        shown = true;

                                        break;
                                    case "FCE014":
                                        //Phone is already registered
                                        Toast toast3 = Toast.makeText(SplashActivity.this, "This phone number is already  registered", Toast.LENGTH_LONG);
                                        toast3.show();
                                        shown = true;
                                        break;
                                }
                                if (!shown) {
                                    Toast toast = Toast.makeText(SplashActivity.this, body, Toast.LENGTH_LONG);
                                    toast.show();
                                    Log.d("ERROR", body);
                                }


                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            );

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private String filterPhone(String phone) {
        phone = phone.replace(" ", "").replace("(", "").replace(")", "");
        if (!phone.startsWith("+")) {
            phone = "+" + phone;
        }
        return phone;
    }
}
