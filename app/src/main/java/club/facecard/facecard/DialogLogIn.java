package club.facecard.facecard;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;


public class DialogLogIn extends Dialog implements
        View.OnClickListener {

    public Activity c;
    private EditText name;

    DialogLogIn(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
    }
    private OnMyDialogResult mDialogResult; // the callback

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_log_in);
        name = findViewById(R.id.name_input);
        Button b = findViewById(R.id.log_in_butt);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               mDialogResult.finish(name.getText().toString(),"stub" /*,phone.getText().toString()*/);

            }
        });


    }


    void setDialogResult(OnMyDialogResult dialogResult){
        mDialogResult = dialogResult;
    }

    @Override
    public void onClick(View v) {

    }


    public interface OnMyDialogResult{
        void finish(String name, String phone);
    }
}
