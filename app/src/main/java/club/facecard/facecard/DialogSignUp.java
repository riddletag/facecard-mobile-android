package club.facecard.facecard;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;


public class DialogSignUp extends Dialog implements
        android.view.View.OnClickListener {

    public Activity c;
    private EditText name;
    private CheckBox accept;
    private boolean expanded = false;

    DialogSignUp(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
    }
    private OnMyDialogResult mDialogResult; // the callback

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_sign_up);
        name = findViewById(R.id.name_input);
        Button b = findViewById(R.id.sign_up_butt);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (accept.isChecked()) mDialogResult.finish(name.getText().toString(),"stub" /*,phone.getText().toString()*/);
else {  Toast toast = Toast.makeText(c, "Please accept the consent", Toast.LENGTH_LONG);
                    toast.show();}
            }
        });
        accept = findViewById(R.id.expandable_text);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!expanded) {
                    accept.setText(R.string.consent_text);
               expanded = true;
                } else {
                    accept.setText(R.string.accept_consent) ;   expanded = false;}
            }
        });
        //     no = (Button) findViewById(R.id.btn_no);
     //   yes.setOnClickListener(this);
     //   no.setOnClickListener(this);

    }


    void setDialogResult(OnMyDialogResult dialogResult){
        mDialogResult = dialogResult;
    }

    @Override
    public void onClick(View v) {

    }


    public interface OnMyDialogResult{
        void finish(String name, String phone);
    }
}
