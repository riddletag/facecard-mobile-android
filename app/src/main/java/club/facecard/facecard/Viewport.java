package club.facecard.facecard;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.ViewGroup;

public class Viewport extends ViewGroup {

    public Viewport(Context context) {
        super(context);
    }

    public Viewport(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public Viewport(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public void onLayout(boolean changed, int left, int top, int right, int bottom) {
    }

    @Override
    public boolean shouldDelayChildPressedState() {
        return false;
    }
    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }
    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);


        int widthj = canvas.getWidth();
        int heightj = canvas.getHeight();
        int right = (widthj / 2)+ ((int)convertDpToPixel(280,getContext()) / 2);
        // -(width / 2)+ (mBoxHeight / 2);
        int left = (widthj / 2)- ((int)convertDpToPixel(280,getContext()) / 2);
        //-(width / 2)+ (mBoxHeight / 2);
        int bottom = (heightj / 2) + ((int)convertDpToPixel(400,getContext()) / 2);
        int top = (heightj / 2)- ((int)convertDpToPixel(400,getContext()) / 2);



        //  int viewportMargin = 0;
        int viewportCornerRadius = 400;
        Paint eraser = new Paint();
        eraser.setAntiAlias(true);
        eraser.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

        //  float width = (float) getWidth() - viewportMargin;
        // float height =  width * (float) 0.7;
        RectF rect = new RectF(left,top,right,bottom);
        // RectF frame = new RectF((float)viewportMargin-2, (float)viewportMargin-2, width+4, height+4);
        //  Path path = new Path();
        Paint stroke = new Paint();
        stroke.setAntiAlias(true);
        stroke.setStrokeWidth(4);
        stroke.setColor(Color.WHITE);
        stroke.setStyle(Paint.Style.STROKE);
        // path.addRoundRect(frame, (float) viewportCornerRadius, (float) viewportCornerRadius, Path.Direction.CW);
        //  canvas.drawPath(path, stroke);
        canvas.drawRoundRect(rect, (float) viewportCornerRadius, (float) viewportCornerRadius, eraser);
    }
}
