package club.facecard.facecard;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;


public class SuccessActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);

        Intent intent = getIntent();
        TextView tv = findViewById(R.id.welcomescreen);

        String name = intent.getStringExtra("name");
        if (name != null && !name.equals("")) {
            String str = "Hi, "+name+"!";
            tv.setText(str);
        }
    }

    @Override
    public void onBackPressed() {
       // super.onPause();
    }

    public void doneButt(View view) {
    }

    public void shareApp(View view) {
    }
}

